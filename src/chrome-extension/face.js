var Face = (function () {
    var settings = {
        cookie_name : '_memdev_env_f90564fe', // a special key
        domains : [
            'lovebus.cc',
            'yingyongying.com',
            'luv-inc.com',
            'memoriki.com',
            'qzoneapp.com'
        ],
        env : {
            production : [ "Production", "P", ""], // env name, badge, badgeColor
            staging    : [ "Staging", "S", ""],
            test       : [ "Test", "T", ""]
        }
    },

    update_status = function (env) {
        if (env in settings.env) {
            var current = settings.env[env];
            chrome.browserAction.setTitle({title:"Current env: " + current[0]});
            chrome.browserAction.setIcon({path: 'icon/'+env+'.png'});
        }
    },
    
    set_env = function (env, bg) {
        var expires = (env == 'production') ? 0 : 2145888000;
        window.localStorage.setItem('env', env);
        for (i=0, j=settings.domains.length; i<j; i++) {
            domain = settings.domains[i];
            chrome.cookies.set({
                url:'http://www.'+ domain +'/',
                name: settings.cookie_name,
                domain : '.' + domain,
                value : env,
                expirationDate : expires
            });
        }
        update_status(env);
        if (!bg) setTimeout(function(){window.close();}, 10);
        return false;
    },
    
    draw = function() {
        var ul = document.createElement('ul'),
        current = get_current_env();
        for (env in settings.env) {
            window.console.log(env);
            var li = document.createElement('li'),
                a = document.createElement('a');

            a.innerHTML = settings.env[env][0];
            a.rel = env;
            a.href = "#";

            if (env == current) a.className = 'on';

            a.onclick = function() {
                set_env(this.rel);
                return false;
            }

            li.appendChild(a);
            ul.appendChild(li);
        }
        document.body.appendChild(ul);
    },

    get_current_env = function() {
        var env = window.localStorage.getItem('env');
        if (env == null) env = 'production';
        return env;
    },

    init = function () {
        set_env(get_current_env(), true);
    }

   ;
   return {
        init : init,
        draw : draw
   }
})();
